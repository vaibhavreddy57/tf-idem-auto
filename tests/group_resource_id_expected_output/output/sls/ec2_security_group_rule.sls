sgr-00140d3e51aad2e43:
  aws.ec2.security_group_rule.present:
  - name: sgr-00140d3e51aad2e43
  - resource_id: sgr-00140d3e51aad2e43
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-04fa08f7b284cd1e9
      UserId: '123456789012'


sgr-00ed93ac23eed55f6:
  aws.ec2.security_group_rule.present:
  - name: sgr-00ed93ac23eed55f6
  - resource_id: sgr-00ed93ac23eed55f6
  - group_id: sg-04fa08f7b284cd1e9
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-01f124742859acf30:
  aws.ec2.security_group_rule.present:
  - name: sgr-01f124742859acf30
  - resource_id: sgr-01f124742859acf30
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 8080
  - to_port: 8080
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-02dcf5ca5bb9218b9:
  aws.ec2.security_group_rule.present:
  - name: sgr-02dcf5ca5bb9218b9
  - resource_id: sgr-02dcf5ca5bb9218b9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1
  - to_port: 65535
  - cidr_ipv4: 172.31.0.0/16
  - tags: []


sgr-0336bcc760f16097a:
  aws.ec2.security_group_rule.present:
  - name: sgr-0336bcc760f16097a
  - resource_id: sgr-0336bcc760f16097a
  - group_id: sg-0d129413d7cea1757
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0d129413d7cea1757
      UserId: '123456789012'


sgr-04299d060ac3ebe2a:
  aws.ec2.security_group_rule.present:
  - name: sgr-04299d060ac3ebe2a
  - resource_id: sgr-04299d060ac3ebe2a
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2376
  - to_port: 2376
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0470200c0811d69df:
  aws.ec2.security_group_rule.present:
  - name: sgr-0470200c0811d69df
  - resource_id: sgr-0470200c0811d69df
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0edb77cb85ac5f73e
      UserId: '123456789012'


sgr-04baf6a7aeec7027c:
  aws.ec2.security_group_rule.present:
  - name: sgr-04baf6a7aeec7027c
  - resource_id: sgr-04baf6a7aeec7027c
  - group_id: sg-0d129413d7cea1757
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e5771dece9c1ed9:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e5771dece9c1ed9
  - resource_id: sgr-05e5771dece9c1ed9
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-05e6ceaaebacf5dc1:
  aws.ec2.security_group_rule.present:
  - name: sgr-05e6ceaaebacf5dc1
  - resource_id: sgr-05e6ceaaebacf5dc1
  - group_id: sg-d68355bf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-d68355bf
      UserId: '123456789012'


sgr-06960cb41fe34b17f:
  aws.ec2.security_group_rule.present:
  - name: sgr-06960cb41fe34b17f
  - resource_id: sgr-06960cb41fe34b17f
  - group_id: sg-0f9910c81ca733164
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06de8bb9a233fe279:
  aws.ec2.security_group_rule.present:
  - name: sgr-06de8bb9a233fe279
  - resource_id: sgr-06de8bb9a233fe279
  - group_id: sg-0edb77cb85ac5f73e
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-06fa8980bf9bffea7:
  aws.ec2.security_group_rule.present:
  - name: sgr-06fa8980bf9bffea7
  - resource_id: sgr-06fa8980bf9bffea7
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      nessus scanner
  - referenced_group_info:
      GroupId: sg-06552329287f9b206
      UserId: '123456789012'


sgr-0746837a711b2f632:
  aws.ec2.security_group_rule.present:
  - name: sgr-0746837a711b2f632
  - resource_id: sgr-0746837a711b2f632
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 1025
  - to_port: 65535
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0748e2625a1c6b9b1:
  aws.ec2.security_group_rule.present:
  - name: sgr-0748e2625a1c6b9b1
  - resource_id: sgr-0748e2625a1c6b9b1
  - group_id: sg-0f9910c81ca733164
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0f9910c81ca733164
      UserId: '123456789012'


sgr-077c2651cc2eb9b1f:
  aws.ec2.security_group_rule.present:
  - name: sgr-077c2651cc2eb9b1f
  - resource_id: sgr-077c2651cc2eb9b1f
  - group_id: sg-070a797a4b433814b
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow pods to communicate with the cluster API Server
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'


sgr-08163ca9869d9fce3:
  aws.ec2.security_group_rule.present:
  - name: sgr-08163ca9869d9fce3
  - resource_id: sgr-08163ca9869d9fce3
  - group_id: sg-01a41f68
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 5432
  - to_port: 5432
  - cidr_ipv4: 24.7.88.198/32
  - tags: []


sgr-0895dc3282b1068e5:
  aws.ec2.security_group_rule.present:
  - name: sgr-0895dc3282b1068e5
  - resource_id: sgr-0895dc3282b1068e5
  - group_id: sg-06552329287f9b206
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-09a323e0c1f512b46:
  aws.ec2.security_group_rule.present:
  - name: sgr-09a323e0c1f512b46
  - resource_id: sgr-09a323e0c1f512b46
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a1c13bf84aef05b3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a1c13bf84aef05b3
  - resource_id: sgr-0a1c13bf84aef05b3
  - group_id: sg-0fc412cebfb987038
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a35370111c0c05fe:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a35370111c0c05fe
  - resource_id: sgr-0a35370111c0c05fe
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 2375
  - to_port: 2375
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a410f396195a1a29:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a410f396195a1a29
  - resource_id: sgr-0a410f396195a1a29
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0a4ef3a78cdce5042:
  aws.ec2.security_group_rule.present:
  - name: sgr-0a4ef3a78cdce5042
  - resource_id: sgr-0a4ef3a78cdce5042
  - group_id: sg-070a797a4b433814b
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0b6d618ed84f17b83:
  aws.ec2.security_group_rule.present:
  - name: sgr-0b6d618ed84f17b83
  - resource_id: sgr-0b6d618ed84f17b83
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 22
  - to_port: 22
  - cidr_ipv4: 10.170.0.0/16
  - tags: []
  - description: Allow bastion to communicate with worker nodes


sgr-0ca3ad84b6ac0506e:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ca3ad84b6ac0506e
  - resource_id: sgr-0ca3ad84b6ac0506e
  - group_id: sg-01a41f68
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0d08ef367d62e82f3:
  aws.ec2.security_group_rule.present:
  - name: sgr-0d08ef367d62e82f3
  - resource_id: sgr-0d08ef367d62e82f3
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 443
  - to_port: 443
  - tags: []
  - description: Allow worker Kubelets and pods to receive communication from the
      cluster control plane
  - referenced_group_info:
      GroupId: sg-070a797a4b433814b
      UserId: '123456789012'


sgr-0e211bb288c7a0d96:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e211bb288c7a0d96
  - resource_id: sgr-0e211bb288c7a0d96
  - group_id: sg-d68355bf
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0e9f9a226e5e1aec6:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e9f9a226e5e1aec6
  - resource_id: sgr-0e9f9a226e5e1aec6
  - group_id: sg-07400e9684a4b7a48
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0eb7727a9d7a24f7d:
  aws.ec2.security_group_rule.present:
  - name: sgr-0eb7727a9d7a24f7d
  - resource_id: sgr-0eb7727a9d7a24f7d
  - group_id: sg-0fc412cebfb987038
  - is_egress: false
  - ip_protocol: tcp
  - from_port: 80
  - to_port: 80
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0ffb22f7ca7a0f16c:
  aws.ec2.security_group_rule.present:
  - name: sgr-0ffb22f7ca7a0f16c
  - resource_id: sgr-0ffb22f7ca7a0f16c
  - group_id: sg-02b4fa0698eaa06cf
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - description: Allow node to communicate with each other
  - referenced_group_info:
      GroupId: sg-02b4fa0698eaa06cf
      UserId: '123456789012'
