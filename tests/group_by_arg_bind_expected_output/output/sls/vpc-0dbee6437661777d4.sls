rtb-0752eb6244ce03cff:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0752eb6244ce03cff
    propagating_vgws: []
    resource_id: rtb-0752eb6244ce03cff
    routes:
    - DestinationCidrBlock: 10.0.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags:
    - Key: Name
      Value: vpc-0dbee6437661777d4
    vpc_id: vpc-0dbee6437661777d4


sg-07400e9684a4b7a48:
  aws.ec2.security_group.present:
  - resource_id: sg-07400e9684a4b7a48
  - name: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - vpc_id: vpc-0dbee6437661777d4
  - tags:
    - Key: Name
      Value: idem-fixture-security-group-eb54958c-7199-4034-8f0f-83a62bcf1dc5
  - description: Created for Idem integration test.


sg-0d129413d7cea1757:
  aws.ec2.security_group.present:
  - resource_id: sg-0d129413d7cea1757
  - name: default
  - vpc_id: vpc-0dbee6437661777d4
  - description: default VPC security group


sgr-0336bcc760f16097a:
  aws.ec2.security_group_rule.present:
  - name: sgr-0336bcc760f16097a
  - resource_id: sgr-0336bcc760f16097a
  - group_id: sg-0d129413d7cea1757
  - is_egress: false
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - tags: []
  - referenced_group_info:
      GroupId: sg-0d129413d7cea1757
      UserId: '123456789012'


sgr-04baf6a7aeec7027c:
  aws.ec2.security_group_rule.present:
  - name: sgr-04baf6a7aeec7027c
  - resource_id: sgr-04baf6a7aeec7027c
  - group_id: sg-0d129413d7cea1757
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


sgr-0e9f9a226e5e1aec6:
  aws.ec2.security_group_rule.present:
  - name: sgr-0e9f9a226e5e1aec6
  - resource_id: sgr-0e9f9a226e5e1aec6
  - group_id: sg-07400e9684a4b7a48
  - is_egress: true
  - ip_protocol: '-1'
  - from_port: -1
  - to_port: -1
  - cidr_ipv4: 0.0.0.0/0
  - tags: []


subnet-05a0fbce3b40dcd05:
  aws.ec2.subnet.present:
  - name: subnet-05a0fbce3b40dcd05
  - resource_id: subnet-05a0fbce3b40dcd05
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.107.0/24
  - availability_zone: eu-west-3b
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz


subnet-085c807f9dcd3bccd:
  aws.ec2.subnet.present:
  - name: subnet-085c807f9dcd3bccd
  - resource_id: subnet-085c807f9dcd3bccd
  - vpc_id: vpc-0dbee6437661777d4
  - cidr_block: 10.0.78.0/24
  - availability_zone: eu-west-3a
  - tags:
    - Key: Name
      Value: aws_ec2_subnet_for_xyz_worker


vpc-0dbee6437661777d4:
  aws.ec2.vpc.present:
  - name: vpc-0dbee6437661777d4
  - resource_id: vpc-0dbee6437661777d4
  - instance_tenancy: default
  - tags:
    - Key: Name
      Value: idem-fixture-vpc-61ddff14-631a-4ee9-bab6-daa191a7da66
  - cidr_block_association_set:
    - AssociationId: vpc-cidr-assoc-054669469e1afac05
      CidrBlock: 10.0.0.0/16
      CidrBlockState:
        State: associated
  - enable_dns_hostnames: false
  - enable_dns_support: true
