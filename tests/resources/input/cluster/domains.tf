# # in support of wavefront-proxies deployed in potato/mango/scaleperf environments
# # proxies in this environment are created via xyz-resources/proxy module
# # wavefront environments use wavefrontProxy = "wavefront-proxy.corp.wavefront.com" in xyz-clusters/.../temp.tfvars
data "aws_route53_zone" "internal-potato-beachops-io" {
  count        = var.profile == "sym-dev4" || var.profile == "sym-prod" || var.profile == "sym-staging" || var.profile == "mango" || var.profile == "scaleperf" ? 1 : 0
  name         = "internal.${replace(var.profile, "/sym-.*/", "potato")}.beachops.io."
  private_zone = true
}

resource "aws_route53_zone_association" "internal-potato-beachops-io" {
  count   = var.create_vpc && var.profile == "sym-dev4" || var.profile == "sym-prod" || var.profile == "sym-staging" || var.profile == "mango" || var.profile == "scaleperf" ? 1 : 0
  zone_id = data.aws_route53_zone.internal-potato-beachops-io[0].zone_id
  vpc_id  = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
}

###########################################################################################################
#                                 Needs to be refactored with new provider                                #
###########################################################################################################
# This is the domain association if the domain is in another account
data "aws_route53_zone" "cross-account-internal-potato-beachops-io" {
  count    = var.cross_account_beachops_domain_profile != "" ? 1 : 0
  provider = aws.cross_account_beachops
  name = "internal.${replace(
    var.cross_account_beachops_domain_profile,
    "/sym-.*/",
    "potato",
  )}.beachops.io."
  private_zone = true
}

resource "null_resource" "cross-account-internal-potato-beachops-io-authorization" {
  count = var.cross_account_beachops_domain_profile != "" ? 1 : 0
  provisioner "local-exec" {
    command = "AWS_PROFILE=${var.cross_account_beachops_domain_profile} aws route53 create-vpc-association-authorization --hosted-zone-id ${data.aws_route53_zone.cross-account-internal-potato-beachops-io[0].zone_id} --vpc VPCRegion=${var.region},VPCId=${aws_vpc.cluster[1].id}"
  }
}

resource "null_resource" "cross-account-internal-potato-beachops-io-association" {
  count = var.cross_account_beachops_domain_profile != "" ? 1 : 0
  provisioner "local-exec" {
    command = "AWS_PROFILE=${var.profile} aws route53 associate-vpc-with-hosted-zone --hosted-zone-id ${data.aws_route53_zone.cross-account-internal-potato-beachops-io[0].zone_id} --vpc VPCRegion=${var.region},VPCId=${aws_vpc.cluster[1].id}"
  }
}

# # For integration with MSK kafka from another account/VPC
data "aws_route53_zone" "msk_kafka_domain" {
  count        = var.msk_kafka_domain ? 1 : 0
  name         = "kafka.${var.region}.amazonaws.com"
  private_zone = true
}

resource "aws_route53_zone_association" "msk_kafka_domain" {
  count        = var.msk_kafka_domain ? 1 : 0
  zone_id      = data.aws_route53_zone.msk_kafka_domain[0].zone_id
  vpc_id       = var.create_vpc ? aws_vpc.cluster[0].id : data.aws_vpc.vpc[0].id
}

module "nested_module1" {
  source      = "nested_module1"
  region      = var.region
  profile     = var.profile
  owner       = var.owner
  clusterName = var.clusterName
  admin_users = var.admin_users
  cogs        = var.cogs
  singleAz    = var.singleAz
}
