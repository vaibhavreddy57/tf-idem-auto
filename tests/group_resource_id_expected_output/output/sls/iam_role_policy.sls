AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole:
  aws.iam.role_policy.present:
  - resource_id: AWS-QuickSetup-StackSet-Local-AdministrationRole-AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - role_name: AWS-QuickSetup-StackSet-Local-AdministrationRole
  - name: AssumeRole-AWS-QuickSetup-StackSet-Local-ExecutionRole
  - policy_document: '{"Statement": [{"Action": ["sts:AssumeRole"], "Effect": "Allow",
      "Resource": ["arn:*:iam::*:role/AWS-QuickSetup-StackSet-Local-ExecutionRole"]}],
      "Version": "2012-10-17"}'


CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468:
  aws.iam.role_policy.present:
  - resource_id: CloudTrail_CloudWatchLogs_Role-oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - role_name: CloudTrail_CloudWatchLogs_Role
  - name: oneClick_CloudTrail_CloudWatchLogs_Role_1522065148468
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream"], "Effect":
      "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailCreateLogStream20141101"}, {"Action": ["logs:PutLogEvents"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:CloudTrail/DefaultLogGroup:log-stream:123456789012_CloudTrail_us-east-1*"],
      "Sid": "AWSCloudTrailPutLogEvents20141101"}], "Version": "2012-10-17"}'


VMWMasterRole-AdministratorAccess:
  aws.iam.role_policy.present:
  - resource_id: VMWMasterRole-AdministratorAccess
  - role_name: VMWMasterRole
  - name: AdministratorAccess
  - policy_document: '{"Statement": [{"Action": "*", "Effect": "Allow", "Resource":
      "*"}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-ExecRolePolicy:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-ExecRolePolicy
  - role_name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: ExecRolePolicy
  - policy_document: '{"Statement": [{"Action": ["logs:*"], "Effect": "Allow", "Resource":
      "arn:aws:logs:*:*:*"}], "Version": "2012-10-17"}'


abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-root:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD-root
  - role_name: abc-sddc-formation-56609b0b-e79-BasicLambdaRole-1G3NFJ9EKTXD
  - name: root
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": "arn:aws:logs:*:*:*"},
      {"Action": ["ec2:DescribeVpcs", "ec2:DescribeRouteTables", "ec2:DescribeSubnets"],
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-ExecRolePolicy:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-ExecRolePolicy
  - role_name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: ExecRolePolicy
  - policy_document: '{"Statement": [{"Action": ["logs:*"], "Effect": "Allow", "Resource":
      "arn:aws:logs:*:*:*"}], "Version": "2012-10-17"}'


abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-root:
  aws.iam.role_policy.present:
  - resource_id: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6-root
  - role_name: abc-sddc-formation-d6763282-9ad-BasicLambdaRole-1LRYHQO6697D6
  - name: root
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents"], "Effect": "Allow", "Resource": "arn:aws:logs:*:*:*"},
      {"Action": ["ec2:DescribeVpcs", "ec2:DescribeRouteTables", "ec2:DescribeSubnets"],
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole-aws-node-simple-http-endpoint-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole-aws-node-simple-http-endpoint-dev-lambda
  - role_name: aws-node-simple-http-endpoint-dev-us-east-1-lambdaRole
  - name: aws-node-simple-http-endpoint-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/aws-node-simple-http-endpoint-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/aws-node-simple-http-endpoint-dev*:*:*"]}],
      "Version": "2012-10-17"}'


bn0lunfd-dev-us-east-1-lambdaRole-bn0lunfd-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: bn0lunfd-dev-us-east-1-lambdaRole-bn0lunfd-dev-lambda
  - role_name: bn0lunfd-dev-us-east-1-lambdaRole
  - name: bn0lunfd-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/bn0lunfd-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/bn0lunfd-dev*:*:*"]}],
      "Version": "2012-10-17"}'


cluster01-InfosecVulnScanRole-cluster01-InfosecVulnScanRole:
  aws.iam.role_policy.present:
  - resource_id: cluster01-InfosecVulnScanRole-cluster01-InfosecVulnScanRole
  - role_name: cluster01-InfosecVulnScanRole
  - name: cluster01-InfosecVulnScanRole
  - policy_document: '{"Statement": [{"Action": "ec2:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": "elasticloadbalancing:Describe*", "Effect": "Allow",
      "Resource": "*"}, {"Action": ["cloudwatch:ListMetrics", "cloudwatch:GetMetricStatistics",
      "cloudwatch:Describe*"], "Effect": "Allow", "Resource": "*"}, {"Action": "autoscaling:Describe*",
      "Effect": "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


cluster01-temp-xyz-cluster-node-cluster01-temp-xyz-bootstrap-access:
  aws.iam.role_policy.present:
  - resource_id: cluster01-temp-xyz-cluster-node-cluster01-temp-xyz-bootstrap-access
  - role_name: cluster01-temp-xyz-cluster-node
  - name: cluster01-temp-xyz-bootstrap-access
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:AssociateAddress",
      "logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


f2scv1hr-dev-us-east-1-lambdaRole-f2scv1hr-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: f2scv1hr-dev-us-east-1-lambdaRole-f2scv1hr-dev-lambda
  - role_name: f2scv1hr-dev-us-east-1-lambdaRole
  - name: f2scv1hr-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/f2scv1hr-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/f2scv1hr-dev*:*:*"]}],
      "Version": "2012-10-17"}'


guardduty-event-role-GuardDuty-Event-IAMPolicy:
  aws.iam.role_policy.present:
  - resource_id: guardduty-event-role-GuardDuty-Event-IAMPolicy
  - role_name: guardduty-event-role
  - name: GuardDuty-Event-IAMPolicy
  - policy_document: '{"Statement": [{"Action": "events:PutEvents", "Effect": "Allow",
      "Resource": "arn:aws:events:*:963874769787:event-bus/default"}], "Version":
      "2012-10-17"}'


hello-world-serverless-dev-us-east-1-lambdaRole-hello-world-serverless-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: hello-world-serverless-dev-us-east-1-lambdaRole-hello-world-serverless-dev-lambda
  - role_name: hello-world-serverless-dev-us-east-1-lambdaRole
  - name: hello-world-serverless-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/hello-world-serverless-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/hello-world-serverless-dev*:*:*"]}],
      "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-node-Amazon_EBS_CSI_Driver:
  aws.iam.role_policy.present:
  - resource_id: idem-test-temp-xyz-cluster-node-Amazon_EBS_CSI_Driver
  - role_name: idem-test-temp-xyz-cluster-node
  - name: Amazon_EBS_CSI_Driver
  - policy_document: '{"Statement": [{"Action": ["ec2:AttachVolume", "ec2:CreateSnapshot",
      "ec2:CreateTags", "ec2:CreateVolume", "ec2:DeleteSnapshot", "ec2:DeleteTags",
      "ec2:DeleteVolume", "ec2:DescribeAvailabilityZones", "ec2:DescribeInstances",
      "ec2:DescribeSnapshots", "ec2:DescribeTags", "ec2:DescribeVolumes", "ec2:DescribeVolumesModifications",
      "ec2:DetachVolume", "ec2:ModifyVolume"], "Effect": "Allow", "Resource": "*"}],
      "Version": "2012-10-17"}'


idem-test-temp-xyz-cluster-node-credstash_xyz_idem-test_access_policy:
  aws.iam.role_policy.present:
  - resource_id: idem-test-temp-xyz-cluster-node-credstash_xyz_idem-test_access_policy
  - role_name: idem-test-temp-xyz-cluster-node
  - name: credstash_xyz_idem-test_access_policy
  - policy_document: '{"Statement": [{"Action": ["kms:GenerateDataKey", "kms:Decrypt"],
      "Effect": "Allow", "Resource": "arn:aws:kms:eu-west-3:123456789012:key/8ac5f341-fd1c-4e9d-9596-8f844dba5cc8"},
      {"Action": ["dynamodb:PutItem", "dynamodb:GetItem", "dynamodb:Query", "dynamodb:Scan"],
      "Effect": "Allow", "Resource": "arn:aws:dynamodb:eu-west-3:123456789012:table/xyz-idem-test-credential-store"}],
      "Version": "2012-10-17"}'


ki5roed2-dev-us-east-1-lambdaRole-ki5roed2-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: ki5roed2-dev-us-east-1-lambdaRole-ki5roed2-dev-lambda
  - role_name: ki5roed2-dev-us-east-1-lambdaRole
  - name: ki5roed2-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/ki5roed2-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/ki5roed2-dev*:*:*"]}],
      "Version": "2012-10-17"}'


masters.training.k8s.local-masters.training.k8s.local:
  aws.iam.role_policy.present:
  - resource_id: masters.training.k8s.local-masters.training.k8s.local
  - role_name: masters.training.k8s.local
  - name: masters.training.k8s.local
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances", "ec2:DescribeRouteTables",
      "ec2:DescribeSecurityGroups", "ec2:DescribeSubnets", "ec2:DescribeVolumes"],
      "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2MasterPermsDescribeResources"},
      {"Action": ["ec2:CreateRoute", "ec2:CreateSecurityGroup", "ec2:CreateTags",
      "ec2:CreateVolume", "ec2:ModifyInstanceAttribute"], "Effect": "Allow", "Resource":
      ["*"], "Sid": "kopsK8sEC2MasterPermsAllResources"}, {"Action": ["ec2:AttachVolume",
      "ec2:AuthorizeSecurityGroupIngress", "ec2:DeleteRoute", "ec2:DeleteSecurityGroup",
      "ec2:DeleteVolume", "ec2:DetachVolume", "ec2:RevokeSecurityGroupIngress"], "Condition":
      {"StringEquals": {"ec2:ResourceTag/KubernetesCluster": "training.k8s.local"}},
      "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2MasterPermsTaggedResources"},
      {"Action": ["autoscaling:DescribeAutoScalingGroups", "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:GetAsgForInstance"], "Effect": "Allow", "Resource": ["*"], "Sid":
      "kopsK8sASMasterPermsAllResources"}, {"Action": ["autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup", "autoscaling:UpdateAutoScalingGroup"],
      "Condition": {"StringEquals": {"autoscaling:ResourceTag/KubernetesCluster":
      "training.k8s.local"}}, "Effect": "Allow", "Resource": ["*"], "Sid": "kopsK8sASMasterPermsTaggedResources"},
      {"Action": ["elasticloadbalancing:AttachLoadBalancerToSubnets", "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
      "elasticloadbalancing:CreateLoadBalancer", "elasticloadbalancing:CreateLoadBalancerPolicy",
      "elasticloadbalancing:CreateLoadBalancerListeners", "elasticloadbalancing:ConfigureHealthCheck",
      "elasticloadbalancing:DeleteLoadBalancer", "elasticloadbalancing:DeleteLoadBalancerListeners",
      "elasticloadbalancing:DescribeLoadBalancers", "elasticloadbalancing:DescribeLoadBalancerAttributes",
      "elasticloadbalancing:DetachLoadBalancerFromSubnets", "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
      "elasticloadbalancing:ModifyLoadBalancerAttributes", "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
      "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer"], "Effect": "Allow",
      "Resource": ["*"], "Sid": "kopsK8sELBMasterPermsRestrictive"}, {"Action": ["iam:ListServerCertificates",
      "iam:GetServerCertificate"], "Effect": "Allow", "Resource": ["*"], "Sid": "kopsMasterCertIAMPerms"},
      {"Action": ["s3:GetBucketLocation", "s3:ListBucket"], "Effect": "Allow", "Resource":
      ["arn:aws:s3:::training.k8s.local"], "Sid": "kopsK8sS3GetListBucket"}, {"Action":
      ["s3:Get*"], "Effect": "Allow", "Resource": "arn:aws:s3:::training.k8s.local/training.k8s.local/*",
      "Sid": "kopsK8sS3MasterBucketFullGet"}, {"Action": ["ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer", "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sECR"}], "Version": "2012-10-17"}'


nodes.training.k8s.local-nodes.training.k8s.local:
  aws.iam.role_policy.present:
  - resource_id: nodes.training.k8s.local-nodes.training.k8s.local
  - role_name: nodes.training.k8s.local
  - name: nodes.training.k8s.local
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeInstances"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sEC2NodePerms"}, {"Action": ["s3:GetBucketLocation",
      "s3:ListBucket"], "Effect": "Allow", "Resource": ["arn:aws:s3:::training.k8s.local"],
      "Sid": "kopsK8sS3GetListBucket"}, {"Action": ["s3:Get*"], "Effect": "Allow",
      "Resource": ["arn:aws:s3:::training.k8s.local/training.k8s.local/addons/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/cluster.spec", "arn:aws:s3:::training.k8s.local/training.k8s.local/config",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/instancegroup/*", "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/issued/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/private/kube-proxy/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/private/kubelet/*",
      "arn:aws:s3:::training.k8s.local/training.k8s.local/pki/ssh/*", "arn:aws:s3:::training.k8s.local/training.k8s.local/secrets/dockerconfig"],
      "Sid": "kopsK8sS3NodeBucketSelectiveGet"}, {"Action": ["ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability", "ecr:GetDownloadUrlForLayer", "ecr:GetRepositoryPolicy",
      "ecr:DescribeRepositories", "ecr:ListImages", "ecr:BatchGetImage"], "Effect":
      "Allow", "Resource": ["*"], "Sid": "kopsK8sECR"}], "Version": "2012-10-17"}'


pvbhullg-dev-us-east-1-lambdaRole-pvbhullg-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: pvbhullg-dev-us-east-1-lambdaRole-pvbhullg-dev-lambda
  - role_name: pvbhullg-dev-us-east-1-lambdaRole
  - name: pvbhullg-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/pvbhullg-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/pvbhullg-dev*:*:*"]}],
      "Version": "2012-10-17"}'


secure-state-event-role-secure-state-event-role-policy:
  aws.iam.role_policy.present:
  - resource_id: secure-state-event-role-secure-state-event-role-policy
  - role_name: secure-state-event-role
  - name: secure-state-event-role-policy
  - policy_document: '{"Statement": [{"Action": "events:PutEvents", "Effect": "Allow",
      "Resource": "arn:aws:events:*:963874769787:event-bus/default"}], "Version":
      "2012-10-17"}'


serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-helloworldRolePolicy0:
  aws.iam.role_policy.present:
  - resource_id: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY-helloworldRolePolicy0
  - role_name: serverlessrepo-hello-world-helloworldRole-BJQ26NJLFUJY
  - name: helloworldRolePolicy0
  - policy_document: '{"Statement": [{"Action": ["ses:SendBounce"], "Effect": "Allow",
      "Resource": "arn:aws:ses:us-east-1:123456789012:identity/test_id"}]}'


service-09vpvytp-dev-us-east-1-lambdaRole-service-09vpvytp-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: service-09vpvytp-dev-us-east-1-lambdaRole-service-09vpvytp-dev-lambda
  - role_name: service-09vpvytp-dev-us-east-1-lambdaRole
  - name: service-09vpvytp-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/service-09vpvytp-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/service-09vpvytp-dev*:*:*"]}],
      "Version": "2012-10-17"}'


t2guydv5-dev-us-east-1-lambdaRole-t2guydv5-dev-lambda:
  aws.iam.role_policy.present:
  - resource_id: t2guydv5-dev-us-east-1-lambdaRole-t2guydv5-dev-lambda
  - role_name: t2guydv5-dev-us-east-1-lambdaRole
  - name: t2guydv5-dev-lambda
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogStream", "logs:CreateLogGroup"],
      "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t2guydv5-dev*:*"]},
      {"Action": ["logs:PutLogEvents"], "Effect": "Allow", "Resource": ["arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/t2guydv5-dev*:*:*"]}],
      "Version": "2012-10-17"}'


xyz-cluster01_redlock_flow_role-xyz-cluster01_redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: xyz-cluster01_redlock_flow_role-xyz-cluster01_redlock_flow_policy
  - role_name: xyz-cluster01_redlock_flow_role
  - name: xyz-cluster01_redlock_flow_policy
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyz-idem-test_redlock_flow_role-xyz-idem-test_redlock_flow_policy:
  aws.iam.role_policy.present:
  - resource_id: xyz-idem-test_redlock_flow_role-xyz-idem-test_redlock_flow_policy
  - role_name: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_policy
  - policy_document: '{"Statement": [{"Action": ["logs:CreateLogGroup", "logs:CreateLogStream",
      "logs:PutLogEvents", "logs:DescribeLogGroups", "logs:DescribeLogStreams"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzClusterRole-idem-test-temp-xyz-bootstrap-access:
  aws.iam.role_policy.present:
  - resource_id: xyzClusterRole-idem-test-temp-xyz-bootstrap-access
  - role_name: xyzClusterRole
  - name: idem-test-temp-xyz-bootstrap-access
  - policy_document: '{"Statement": [{"Action": ["ec2:CreateTags", "ec2:AssociateAddress",
      "logs:CreateLogGroup", "logs:CreateLogStream", "logs:PutLogEvents"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics:
  aws.iam.role_policy.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-PolicyCloudWatchMetrics
  - policy_document: '{"Statement": [{"Action": ["cloudwatch:PutMetricData"], "Effect":
      "Allow", "Resource": "*"}], "Version": "2012-10-17"}'


xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions:
  aws.iam.role_policy.present:
  - resource_id: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY-xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions
  - role_name: xyzctl-pr-ssc-xyz-poc-cluster-ServiceRole-1889QFJ9Q8DJY
  - name: xyzctl-pr-ssc-xyz-poc-cluster-PolicyELBPermissions
  - policy_document: '{"Statement": [{"Action": ["ec2:DescribeAccountAttributes",
      "ec2:DescribeAddresses", "ec2:DescribeInternetGateways"], "Effect": "Allow",
      "Resource": "*"}], "Version": "2012-10-17"}'
