rtb-01e542a8c56c9511f:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0f2b91e5f78d7af47
      RouteTableId: rtb-01e542a8c56c9511f
      SubnetId: subnet-09cecc8c853637d3b
    name: rtb-01e542a8c56c9511f
    propagating_vgws: []
    resource_id: rtb-01e542a8c56c9511f
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Environment
      Value: test-dev
    - Key: Name
      Value: idem-test-xyz-public-0
    - Key: Automation
      Value: 'true'
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0445912793473da66:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-05b12a843fea97f87
      RouteTableId: rtb-0445912793473da66
      SubnetId: subnet-0094b72dfb7ce6131
    name: rtb-0445912793473da66
    propagating_vgws: []
    resource_id: rtb-0445912793473da66
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test-xyz-public-1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    vpc_id: vpc-0738f2a523f4735bd


rtb-0516e0e06d933d9f4:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-080a96c3caa4d67bc
      RouteTableId: rtb-0516e0e06d933d9f4
      SubnetId: subnet-050732fa4616470d9
    name: rtb-0516e0e06d933d9f4
    propagating_vgws: []
    resource_id: rtb-0516e0e06d933d9f4
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0a49a65a4bb87370a
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Environment
      Value: test-dev
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-0
    - Key: Owner
      Value: org1
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


rtb-05bd8f7251c25d82c:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0e7f80eb1863284da
      RouteTableId: rtb-05bd8f7251c25d82c
      SubnetId: subnet-0d68d61b1ab708d42
    name: rtb-05bd8f7251c25d82c
    propagating_vgws: []
    resource_id: rtb-05bd8f7251c25d82c
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-0eee9bba485b312a8
      Origin: CreateRoute
      State: active
    tags:
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Owner
      Value: org1
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    - Key: Name
      Value: idem-test-xyz-public-2
    - Key: Environment
      Value: test-dev
    vpc_id: vpc-0738f2a523f4735bd


rtb-0752eb6244ce03cff:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0752eb6244ce03cff
    propagating_vgws: []
    resource_id: rtb-0752eb6244ce03cff
    routes:
    - DestinationCidrBlock: 10.0.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags:
    - Key: Name
      Value: vpc-0dbee6437661777d4
    vpc_id: vpc-0dbee6437661777d4


rtb-0b0a3c628c59ac049:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0ef8af8654a7c1fb3
      RouteTableId: rtb-0b0a3c628c59ac049
      SubnetId: subnet-039e53122e038d38c
    name: rtb-0b0a3c628c59ac049
    propagating_vgws: []
    resource_id: rtb-0b0a3c628c59ac049
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-076cd14a28acd21b4
      Origin: CreateRoute
      State: active
    tags:
    - Key: COGS
      Value: OPEX
    - Key: Owner
      Value: org1
    - Key: Name
      Value: idem-test-xyz-private-2
    - Key: Environment
      Value: test-dev
    - Key: Automation
      Value: 'true'
    - Key: KubernetesCluster
      Value: idem-test
    vpc_id: vpc-0738f2a523f4735bd


rtb-0e8c5df719166603e:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0e8c5df719166603e
    propagating_vgws: []
    resource_id: rtb-0e8c5df719166603e
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-0738f2a523f4735bd


rtb-0ee77d1deb1a1d86a:
  aws.ec2.route_table.present:
  - associations:
    - AssociationState:
        State: associated
      Main: false
      RouteTableAssociationId: rtbassoc-0832ce3bb146df047
      RouteTableId: rtb-0ee77d1deb1a1d86a
      SubnetId: subnet-05dfaa0d01a337199
    name: rtb-0ee77d1deb1a1d86a
    propagating_vgws: []
    resource_id: rtb-0ee77d1deb1a1d86a
    routes:
    - DestinationCidrBlock: 10.170.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: nat-0c02a1f1d590b5534
      Origin: CreateRoute
      State: active
    tags:
    - Key: Owner
      Value: org1
    - Key: KubernetesCluster
      Value: idem-test
    - Key: Name
      Value: idem-test-xyz-private-1
    - Key: Environment
      Value: test-dev
    - Key: COGS
      Value: OPEX
    - Key: Automation
      Value: 'true'
    vpc_id: vpc-0738f2a523f4735bd


rtb-0f840e96646e44c53:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-0f840e96646e44c53
    propagating_vgws: []
    resource_id: rtb-0f840e96646e44c53
    routes:
    - DestinationCidrBlock: 10.1.150.0/28
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    tags: []
    vpc_id: vpc-08f76fe175071e969


rtb-6bcc0d02:
  aws.ec2.route_table.present:
  - associations: []
    name: rtb-6bcc0d02
    propagating_vgws: []
    resource_id: rtb-6bcc0d02
    routes:
    - DestinationCidrBlock: 172.31.0.0/16
      GatewayId: local
      Origin: CreateRouteTable
      State: active
    - DestinationCidrBlock: 0.0.0.0/0
      GatewayId: igw-89798ae0
      Origin: CreateRoute
      State: active
    tags:
    - Key: Name
      Value: idem-test
    vpc_id: vpc-dcae57b5
