import difflib
import filecmp
import os.path
import shutil


def compare_folders_for_equality(hub, output_dir_path, expected_output_dir_path):
    try:
        diff = "Below files have unexpected output\n"
        list_of_dir_in_expected_output_path = os.listdir(expected_output_dir_path)
        list_of_dir_in_output_path = os.listdir(output_dir_path)
        assert list_of_dir_in_output_path == list_of_dir_in_expected_output_path
        diff_found = False
        for (path1, folders1, files1), (path2, folders2, files2) in zip(
            os.walk(expected_output_dir_path), os.walk(output_dir_path)
        ):
            assert folders1 == folders2
            assert files1 == files2
            for file in files1:
                file1_path = f"{path1}/{file}"
                file2_path = f"{path2}/{file}"
                if not filecmp.cmp(file1_path, file2_path):
                    diff = f"{diff}\n***********{file1_path}**************"
                    file1_data = open(file1_path).readlines()
                    file2_data = open(file2_path).readlines()
                    for line in difflib.unified_diff(file1_data, file2_data):
                        diff = diff + line
                    diff_found = True
        assert not diff_found, diff
    finally:
        if not hub.test.idem_codegen.persist_output:
            for filename in os.listdir(output_dir_path):
                file_path = os.path.join(output_dir_path, filename)
                try:
                    if os.path.isfile(file_path) or os.path.islink(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print(f"Failed to delete {file_path}. Reason: {e}")
